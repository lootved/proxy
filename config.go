package main

type hostConfig struct {
	host  string
	ports map[string]string
}

var envMap = map[string]hostConfig{
	"dev": {
		host:  "h1",
		ports: map[string]string{"8443": "443"},
	},
	"sshdev": {
		host:  "h2",
		ports: map[string]string{"2222": "80"},
	},
	"rd": {
		host:  "h3",
		ports: map[string]string{"1822": "8443"},
	},
}
