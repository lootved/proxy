package main

import (
	"bytes"
	"flag"
	"io"
	"log"
	"net"
	"sync"
)

const localhost = "0.0.0.0"

var showIncoming bool
var showOutgoing bool

func main() {
	var env string
	flag.StringVar(&env, "env", "dev", "defines the target url")
	flag.BoolVar(&showIncoming, "in", false, "show the payload being forwarded to the remote host")
	flag.BoolVar(&showOutgoing, "out", false, "show the payload coming from the remote host")
	flag.Parse()
	log.Println("Started proxy with env:", env)
	redirectPorts(env)
}

func redirectPorts(env string) {

	cfg, validEnv := envMap[env]
	if !validEnv {
		log.Fatalln("Invalid env supplied")
	}
	log.Println("remote:", cfg.host)

	var wg sync.WaitGroup

	for hostPort, remotePort := range cfg.ports {
		go redirectPortWorker(localhost+":"+hostPort, cfg.host+":"+remotePort, &wg)
		wg.Add(1)
	}
	wg.Wait()
}

func redirectPortWorker(localHost, remoteHost string, wg *sync.WaitGroup) {
	defer wg.Done()

	ln, err := net.Listen("tcp", localHost)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("redirecting", localHost, "-->", remoteHost)
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go handleConnection(conn, remoteHost)
	}
}

func forward(targetHostStr string, show bool) forwarder {

	//targetHost := []byte(targetHostStr)
	//hostSize := len(targetHost)

	return func(src, dst net.Conn) {
		defer src.Close()
		defer dst.Close()

		if !show {
			io.Copy(dst, src)
		} else {
			var buf bytes.Buffer
			tee := io.TeeReader(src, &buf)
			io.Copy(dst, tee)
			log.Println(buf.String())
			log.Println("-----------------------")
		}
	}
}

type forwarder func(src, dst net.Conn)

func handleConnection(c net.Conn, remoteHostPort string) {
	log.Println("Connection from : ", c.RemoteAddr(), "to", remoteHostPort)
	remote, err := net.Dial("tcp", remoteHostPort)
	if err != nil {
		log.Println(err)
		return
	}
	go forward(remoteHostPort, showIncoming)(c, remote)
	go forward(remoteHostPort, showOutgoing)(remote, c)
}
